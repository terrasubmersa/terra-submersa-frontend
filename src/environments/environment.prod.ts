export const environment = {
  production: true,
  apiOSM: '/data/osm-tiles',
  envName: 'production',
  apiTileBackend: '/tiles/api'
};
