// local properties to tie the angular aplciation to a local blowned Play backend
// ng serve --environment=dev-local-backend

export const environment = {
  production: false,
  apiOSM: '/assets/dev-data/osm-tiles',
  apiTileBackend: 'http://localhost:9000'
};
