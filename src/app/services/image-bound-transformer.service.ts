import {Injectable} from '@angular/core';
import {ImageBoundTransformation} from '../models/image-bound-transformation';
import {LatLonCoords} from '../models/lat-lon-coord';
import {LatLonCoordsCalculatorService} from './lat-lon-coords-calculator.service';

/**
 * manage transformation for ImageBoundTransformation
 * i.e. what happen when the side is dragded to a new position
 */

@Injectable()
export class ImageBoundTransformerService {
  readonly calcService: LatLonCoordsCalculatorService;

  constructor() {
    this.calcService = new LatLonCoordsCalculatorService();
  }

  moveRight(transfo: ImageBoundTransformation, dragPoint: LatLonCoords): ImageBoundTransformation {
    const dragPointBackRotated = this.calcService.rotateWithCenter(transfo.center, -transfo.rotation, dragPoint);

    const newWidth = transfo.widthLon / 2 + dragPointBackRotated.lon - transfo.center.lon;
    const newX = transfo.center.lon + (newWidth - transfo.widthLon) / 2;
    const rotatedCenter = this.calcService.rotateWithCenter(transfo.center, transfo.rotation, new LatLonCoords(transfo.center.lat, newX));

    return new ImageBoundTransformation(rotatedCenter, transfo.widthLat, newWidth, transfo.rotation);
  }

  moveLeft(transfo: ImageBoundTransformation, dragPoint: LatLonCoords): ImageBoundTransformation {
    const dragPointBackRotated = this.calcService.rotateWithCenter(transfo.center, -transfo.rotation, dragPoint);

    const newWidth = transfo.widthLon / 2 - dragPointBackRotated.lon + transfo.center.lon;
    const newX = transfo.center.lon + (transfo.widthLon - newWidth ) / 2;
    const rotatedCenter = this.calcService.rotateWithCenter(transfo.center, transfo.rotation, new LatLonCoords(transfo.center.lat, newX));

    return new ImageBoundTransformation(rotatedCenter, transfo.widthLat, newWidth, transfo.rotation);
  }

  moveBottom(transfo: ImageBoundTransformation, dragPoint: LatLonCoords): ImageBoundTransformation {
    const dragPointBackRotated = this.calcService.rotateWithCenter(transfo.center, -transfo.rotation, dragPoint);

    const newHeight = transfo.widthLat / 2 - dragPointBackRotated.lat + transfo.center.lat;
    const newY = transfo.center.lat + (transfo.widthLat - newHeight) / 2;
    const rotatedCenter = this.calcService.rotateWithCenter(transfo.center, transfo.rotation, new LatLonCoords(newY, transfo.center.lon));

    return new ImageBoundTransformation(rotatedCenter, newHeight, transfo.widthLon, transfo.rotation);
  }

  moveTop(transfo: ImageBoundTransformation, dragPoint: LatLonCoords): ImageBoundTransformation {
    const dragPointBackRotated = this.calcService.rotateWithCenter(transfo.center, -transfo.rotation, dragPoint);

    const newHeight = transfo.widthLat / 2 + dragPointBackRotated.lat - transfo.center.lat;
    const newY = transfo.center.lat + (newHeight - transfo.widthLat) / 2;
    const rotatedCenter = this.calcService.rotateWithCenter(transfo.center, transfo.rotation, new LatLonCoords(newY, transfo.center.lon));

    return new ImageBoundTransformation(rotatedCenter, newHeight, transfo.widthLon, transfo.rotation);
  }

  rotateTop(transfo: ImageBoundTransformation, dragPoint: LatLonCoords): ImageBoundTransformation {
    const dLat = dragPoint.lat - transfo.center.lat;
    const dLon = -dragPoint.lon + transfo.center.lon;
    let alpha = Math.acos(dLat / Math.sqrt(dLat ** 2 + dLon ** 2));
    if (dLon < 0) {
      alpha = -alpha;
    }
    alpha = alpha * 180 / Math.PI;
    return new ImageBoundTransformation(transfo.center, transfo.widthLat, transfo.widthLon, alpha);
  }

}
