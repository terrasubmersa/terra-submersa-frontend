import {Injectable} from '@angular/core';
import {MapLayerDescription} from '../models/map-layer-description';
import {AvailableMapLayers} from '../models/available-map-layers';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import * as _ from 'lodash';
import {TileSystemBoundaries} from '../models/tile-system-boundaries';

@Injectable()
export class DataLayersService {
  private baseLayer: MapLayerDescription;

  constructor(private http: HttpClient) {
    this.baseLayer = new MapLayerDescription(
      'openstreetmap.landscape',
      'https://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey=8ec51d8024c2405fb9bc825df102046a',
      'OpenStreet map landscape',
      'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
      '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="http://mapbox.com">Mapbox</a>',
      new TileSystemBoundaries(1, 19, undefined)
    )
  }

  geMaptLayerDescriptionList(): Promise<AvailableMapLayers> {
    const self = this;
    return this.http.get(environment.apiTileBackend + '/systems')
      .toPromise()
      .then(res => {
        const layers = _.map(res, (resObj: any) => {
          return new MapLayerDescription(
            resObj.id,
            environment.apiOSM + resObj.urlMask,
            resObj.description,
            resObj.copyright,
            resObj.boundaries as TileSystemBoundaries
            // environment.apiOSM + '/images/heli11/{z}/{x}/{y}.png'
          )
        });
        layers.unshift(self.baseLayer);
        return new AvailableMapLayers(layers);
      });
  }
}
