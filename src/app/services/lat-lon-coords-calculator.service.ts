import {Injectable} from '@angular/core';
import {LatLonCoords} from '../models/lat-lon-coord';

const deg2rad = (alpha: number) => {
  return alpha * Math.PI / 180;
};

@Injectable()
export class LatLonCoordsCalculatorService {

  constructor() {
  }


  /**
   * rotate a coordinates around a given center, with a given angle
   * @param {LatLonCoords} center the point around which the roatation takes place
   * @param {number} alpha angle in degrees
   * @param {LatLonCoords} coords the point to be rotated
   * @return {LatLonCoords}
   */
  rotateWithCenter(center: LatLonCoords, alpha: number, coords: LatLonCoords): LatLonCoords {
    const alphaRad = deg2rad(alpha);
    return new LatLonCoords(
      center.lat + (coords.lon - center.lon) * Math.sin(alphaRad) + (coords.lat - center.lat) * Math.cos(alphaRad),
      center.lon + (coords.lon - center.lon) * Math.cos(alphaRad) - (coords.lat - center.lat) * Math.sin(alphaRad)
    );
  }

}
