import { TestBed, inject } from '@angular/core/testing';

import { DataLayersService } from './data-layers.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('DataLayersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataLayersService],
      imports: [
        HttpClientTestingModule,
      ]
    });
  });

  it('should be created', inject([HttpClient, DataLayersService], (http: HttpClient, service: DataLayersService) => {
    expect(service).toBeTruthy();
  }));
});
