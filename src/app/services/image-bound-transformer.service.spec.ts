import {TestBed, inject} from '@angular/core/testing';

import {ImageBoundTransformerService} from './image-bound-transformer.service';
import {ImageBoundTransformation} from '../models/image-bound-transformation';
import {LatLonCoords} from '../models/lat-lon-coord';

describe('ImageBoundTransformerService', () => {
  let service: ImageBoundTransformerService;

  function assertCloseIBT(actual: ImageBoundTransformation, expected: ImageBoundTransformation) {
    expect(actual.center.lat).toBeCloseTo(expected.center.lat, 0.000001);
    expect(actual.center.lon).toBeCloseTo(expected.center.lon, 0.000001);
    expect(actual.widthLat).toBeCloseTo(expected.widthLat, 0.000001);
    expect(actual.widthLon).toBeCloseTo(expected.widthLon, 0.000001);
    expect(actual.rotation).toBeCloseTo(expected.rotation, 0.000001);
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImageBoundTransformerService]
    });
  });

  beforeEach((inject([ImageBoundTransformerService], (sv: ImageBoundTransformerService) => {
        service = sv;
      }
    )
  ));
  it('service should be created by beforeEach', () => {
    expect(service).toBeTruthy();
  });


  describe('moveRight', () => {
    describe('0° rotation', () => {
      const ibtSource = new ImageBoundTransformation(new LatLonCoords(60, 30), 40, 20, 0);

      describe('no move', () => {
        it('mid side', () => {
          const dragPoint = ibtSource.pointRight(0);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
        it('along side', () => {
          const dragPoint = ibtSource.pointRight(0.3);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
      });
      describe('outer move', () => {
        it('mid side', () => {
          const dragPoint = new LatLonCoords(60, 80);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(60, 50), 40, 60, 0));
        });
        it('along side', () => {
          const dragPoint = new LatLonCoords(10, 80);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(60, 50), 40, 60, 0));
        });
      });
      describe('inner move', () => {
        it('mid side', () => {
          const dragPoint = new LatLonCoords(60, 20);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(60, 20), 40, 0, 0));
        });
        it('along side', () => {
          const dragPoint = new LatLonCoords(10, 20);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(60, 20), 40, 0, 0));
        });
      });
    });

    describe('-30° rotation', () => {
      const ibtSource = new ImageBoundTransformation(new LatLonCoords(60, 30), 40, 20, -30);

      describe('no move', () => {
        it('mid side', () => {
          const dragPoint = ibtSource.pointRight(0);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
        it('along side', () => {
          const dragPoint = ibtSource.pointRight(0.3);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
      });

      describe('outer move', () => {
        it('out', () => {
          const dragPoint = new LatLonCoords(60, 80);

          const ibtTarget = service.moveRight(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(
            new LatLonCoords(51.67468245269452, 44.41987298107781),
            40,
            53.30127018922194,
            -30));
        });
      });
    });
  });

  describe('moveLeft', () => {
    describe('0° rotation', () => {
      const ibtSource = new ImageBoundTransformation(new LatLonCoords(60, 30), 40, 20, 0);

      describe('no move', () => {
        it('mid side', () => {
          const dragPoint = ibtSource.pointLeft(0);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
        it('along side', () => {
          const dragPoint = ibtSource.pointLeft(0.3);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
      });
      describe('outer move', () => {
        it('mid side', () => {
          const dragPoint = new LatLonCoords(60, 10);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(60, 25), 40, 30, 0));
        });
        it('along side', () => {
          const dragPoint = new LatLonCoords(0, 10);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(60, 25), 40, 30, 0));
        });
      });
      describe('inner move', () => {
        it('mid side', () => {
          const dragPoint = new LatLonCoords(60, 30);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(60, 35), 40, 10, 0));
        });
        it('along side', () => {
          const dragPoint = new LatLonCoords(10, 30);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(60, 35), 40, 10, 0));
        });
      });
    });

    describe('-30° rotation', () => {
      const ibtSource = new ImageBoundTransformation(new LatLonCoords(60, 30), 40, 20, -30);

      describe('no move', () => {
        it('mid side', () => {
          const dragPoint = ibtSource.pointLeft(0);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          assertCloseIBT(ibtTarget, ibtSource);
        });
        it('along side', () => {
          const dragPoint = ibtSource.pointLeft(0.3);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          assertCloseIBT(ibtTarget, ibtSource);
        });
      });

      describe('outer move', () => {
        it('out', () => {
          const dragPoint = new LatLonCoords(60, 10);

          const ibtTarget = service.moveLeft(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(
            new LatLonCoords(61.83012701892219, 26.83012701892219),
            40,
            27.320508075688775,
            -30));
        });
      });
    });
  });

  describe('moveTop', () => {
    describe('0° rotation', () => {
      const ibtSource = new ImageBoundTransformation(new LatLonCoords(60, 30), 40, 20, 0);

      describe('no move', () => {
        it('mid side', () => {
          const dragPoint = ibtSource.pointTop(0);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
        it('along side', () => {
          const dragPoint = ibtSource.pointTop(0.3);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
      });
      describe('outer move', () => {
        it('mid side', () => {
          const dragPoint = new LatLonCoords(100, 30);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(70, 30), 60, 20, 0));
        });
        it('along side', () => {
          const dragPoint = new LatLonCoords(100, 10);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(70, 30), 60, 20, 0));
        });
      });
      describe('inner move', () => {
        it('mid side', () => {
          const dragPoint = new LatLonCoords(40, 30);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(40, 30), 0, 20, 0));
        });
        it('along side', () => {
          const dragPoint = new LatLonCoords(40, 20);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(40, 30), 0, 20, 0));
        });
      });
    });

    describe('-30° rotation', () => {
      const ibtSource = new ImageBoundTransformation(new LatLonCoords(60, 30), 40, 20, -30);

      describe('no move', () => {
        it('mid side', () => {
          const dragPoint = ibtSource.pointTop(0);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
        it('along side', () => {
          const dragPoint = ibtSource.pointTop(0.3);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
      });

      describe('outer move', () => {
        it('out', () => {
          const dragPoint = new LatLonCoords(80, 30);

          const ibtTarget = service.moveTop(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(
            new LatLonCoords(58.83974596215562, 29.330127018922195),
            37.32050807568878,
            20,
            -30));
        });
      });
    });
  });

  describe('moveBottom', () => {
    describe('0° rotation', () => {
      const ibtSource = new ImageBoundTransformation(new LatLonCoords(60, 30), 40, 20, 0);

      describe('no move', () => {
        it('mid side', () => {
          const dragPoint = ibtSource.pointBottom(0);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
        it('along side', () => {
          const dragPoint = ibtSource.pointBottom(0.3);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(ibtSource);
        });
      });
      describe('outer move', () => {
        it('mid side', () => {
          const dragPoint = new LatLonCoords(30, 30);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(55, 30), 50, 20, 0));
        });
        it('along side', () => {
          const dragPoint = new LatLonCoords(30, 80);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(55, 30), 50, 20, 0));
        });
      });
      describe('inner move', () => {
        it('mid side', () => {
          const dragPoint = new LatLonCoords(80, 30);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(80, 30), 0, 20, 0));
        });
        it('along side', () => {
          const dragPoint = new LatLonCoords(80, 20);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(new LatLonCoords(80, 30), 0, 20, 0));
        });
      });
    });

    describe('-30° rotation', () => {
      const ibtSource = new ImageBoundTransformation(new LatLonCoords(60, 30), 40, 20, -30);

      describe('no move', () => {
        it('mid side', () => {
          const dragPoint = ibtSource.pointBottom(0);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          assertCloseIBT(ibtTarget, ibtSource);
        });
        it('along side', () => {
          const dragPoint = ibtSource.pointBottom(0.3);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          assertCloseIBT(ibtTarget, ibtSource);
        });
      });

      describe('outer move', () => {
        it('out', () => {
          const dragPoint = new LatLonCoords(80, 30);

          const ibtTarget = service.moveBottom(ibtSource, dragPoint);

          expect(ibtTarget).toEqual(new ImageBoundTransformation(
            new LatLonCoords(76.1602540378444, 39.3301270189222),
            2.679491924311222,
            20,
            -30));
        });
      });
    });
  });
});

