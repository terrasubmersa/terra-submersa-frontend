import {LatLonCoordsCalculatorService} from './lat-lon-coords-calculator.service';
import {LatLonCoords} from '../models/lat-lon-coord';


describe('LatLonCoordsCalculatorService', () => {

  const service = new LatLonCoordsCalculatorService();

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  function assertCloseCoords(actual: LatLonCoords, expected: LatLonCoords) {
    expect(actual.lat).toBeCloseTo(expected.lat, 0.001);
    expect(actual.lon).toBeCloseTo(expected.lon, 0.001);

  }

  describe('center on 0, 0', () => {
    const center = new LatLonCoords(0, 0);
    it('no rotation', () => {
      const llc = new LatLonCoords(20, 30);

      const llcRotated = service.rotateWithCenter(center, 0, llc);

      expect(llcRotated).toEqual(llc);
    });

    it('+30°', () => {
      const llc = new LatLonCoords(20, 30);

      const llcRotated = service.rotateWithCenter(center, 30, llc);

      assertCloseCoords(llcRotated, new LatLonCoords(32.32050808, 15.98076211));
    });

    it('-30°', () => {
      const llc = new LatLonCoords(20, 30);

      const llcRotated = service.rotateWithCenter(center, -30, llc);

      assertCloseCoords(llcRotated, new LatLonCoords(2.320508076, 35.98076211));
    });
  });

  describe('center on point', () => {
    const center = new LatLonCoords(5, 10);
    it('no rotation', () => {

      const llcRotated = service.rotateWithCenter(center, 0, center);

      expect(llcRotated).toEqual(center);
    });

    it('+30°', () => {
      const llcRotated = service.rotateWithCenter(center, 30, center);

      assertCloseCoords(llcRotated, center);
    });

    it('-30°', () => {
      const llcRotated = service.rotateWithCenter(center, -30, center);

      assertCloseCoords(llcRotated, center);
    });
  });

  describe('center on 5, 10', () => {
    const center = new LatLonCoords(5, 10);
    it('no rotation', () => {
      const llc = new LatLonCoords(20, 30);

      const llcRotated = service.rotateWithCenter(center, 0, llc);

      expect(llcRotated).toEqual(llc);
    });

    it('+30°', () => {
      const llc = new LatLonCoords(20, 30);

      const llcRotated = service.rotateWithCenter(center, 30, llc);

      assertCloseCoords(llcRotated, new LatLonCoords(27.99038106, 19.82050808));
    });

    it('-30°', () => {
      const llc = new LatLonCoords(20, 30);

      const llcRotated = service.rotateWithCenter(center, -30, llc);

      assertCloseCoords(llcRotated, new LatLonCoords(7.990381057, 34.82050808));
    });
  });
});
