import {Component} from '@angular/core';
import {DataLayersService} from './services/data-layers.service';
import * as SelectedMapLayersActions from './store/selected-map-layers-actions';
import {Store} from '@ngrx/store';
import {AppState} from './AppState';
import {AvailableMapLayers} from './models/available-map-layers';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataLayersService]
})
export class AppComponent {
  title = 'Terra Submersa data viewer';
  constructor(private dataLayersService: DataLayersService, private store: Store<AppState>) {
    dataLayersService.geMaptLayerDescriptionList()
      .then((aml: AvailableMapLayers) => {
        this.store.dispatch(new SelectedMapLayersActions.SetAvailableLayers(aml));
        this.store.dispatch(new SelectedMapLayersActions.SelectAll());
      });
  }
}
