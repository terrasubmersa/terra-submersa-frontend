import {Component, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {Observable} from 'rxjs/Observable';
import {AppState} from '../../AppState';
import {Store} from '@ngrx/store';
import {SelectedMapLayers} from '../../models/selected-map-layers';
import {MapLayerDescription} from '../../models/map-layer-description';
import {AdjustableImage} from '../../leaflet/adjustable-image';
import {ImageBoundTransformation} from '../../models/image-bound-transformation';
import {LatLonCoords} from '../../models/lat-lon-coord';

@Component({
  selector: 'ts-map-base',
  templateUrl: './map-base.component.html',
  styleUrls: ['./map-base.component.css']
})
export class MapBaseComponent implements OnInit {
  map;
  osbSelectedMapLayers: Observable<SelectedMapLayers>;
  leafletMapLayers: Map<string, any> = new Map();

  constructor(private store: Store<AppState>) {
    this.osbSelectedMapLayers = store.select('selectedMapLayers');
  }

  ngOnInit() {
    const self = this;
    self.map = L.map('mapid');
    self.map.setView([37.42, 23.12], 14);

    L.control.scale({metric: true, position: 'bottomright'}).addTo(self.map);

    self.osbSelectedMapLayers.subscribe((xs) => {
      this.updateSelectedMapLayers(xs);
    });

    // const adjustableImage = new AdjustableImage('/assets/dev-data/snapshots/koilada_utm34n-contours-snapshot.png',
    //   new ImageBoundTransformation(
    //     new LatLonCoords(37.42516275, 23.11749779),
    //     0.053229034,
    //     0.032387094,
    //     0
    //   ));
    // adjustableImage.addToLayer(self.map);
  }

  updateSelectedMapLayers(sml: SelectedMapLayers) {
    const self = this;

    sml.availableMapLayers.list.forEach((ld) => {
      if (self.leafletMapLayers.has(ld.id)) {
        self.removeMapLayer(ld.id);
      }
    });
    sml.availableMapLayers.list.forEach((ld) => {
      if (sml.isSelected(ld.id)) {
        self.addMapLayer(ld);
      }
    });

  }

  getSelectedMapLayers(): Map<string, any> {
    return this.leafletMapLayers;
  }

  addMapLayer(ld: MapLayerDescription) {
    const self = this;

    self.leafletMapLayers.set(ld.id, L.tileLayer(ld.osmPath, {
      maxZoom: ld.boundaries.maxLevel,
      attribution: ld.copyright,
      id: ld.id
    }));
    self.leafletMapLayers.get(ld.id).addTo(self.map);
  }

  removeMapLayer(id: string) {
    const self = this;

    self.map.removeLayer(self.leafletMapLayers.get(id));
    self.leafletMapLayers.delete(id);
  }
}
