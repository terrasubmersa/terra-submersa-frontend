import {Component, NgZone, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {SelectedMapLayers} from '../../models/selected-map-layers';
import {Store} from '@ngrx/store';
import {AppState} from '../../AppState';
import {MdDialogRef} from '@angular/material';
import * as SelectedMapLayersActions from '../../store/selected-map-layers-actions';

@Component({
  selector: 'ts-map-layer-selector',
  templateUrl: './map-layer-selector.component.html',
  styleUrls: ['./map-layer-selector.component.css']
})
export class MapLayerSelectorComponent implements OnInit {
  obsSelectedMapLayers: Observable<SelectedMapLayers>;
  selectedMapLayers: SelectedMapLayers;
  selectedIds = {};

  constructor(public dialogRef: MdDialogRef<MapLayerSelectorComponent>,
              private store: Store<AppState>,
              private zone: NgZone) {
    this.obsSelectedMapLayers = store.select('selectedMapLayers');
  }

  ngOnInit() {
    const self = this;

    self.obsSelectedMapLayers.subscribe((sml) => {
      self.selectedMapLayers = sml;
      self.selectedMapLayers.selectedIds.forEach((id) => self.selectedIds[id] = true);
    })
  }

  changeToggle(id, checked) {
    this.store.dispatch(new SelectedMapLayersActions.SelectState(id, checked));
  }

}
