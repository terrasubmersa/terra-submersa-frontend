import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TsHeaderComponent} from './ts-header.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {importedModules} from '../../imported-modules';

describe('TsHeaderComponent', () => {
  let component: TsHeaderComponent;
  let fixture: ComponentFixture<TsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TsHeaderComponent],
      imports: importedModules,
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
