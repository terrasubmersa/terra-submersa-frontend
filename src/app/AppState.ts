import {AvailableMapLayers} from './models/available-map-layers';
import {SelectedMapLayers} from './models/selected-map-layers';
/**
 * the application state for the Redux Store
 */
export interface AppState {
  selectedMapLayers: SelectedMapLayers;
}
