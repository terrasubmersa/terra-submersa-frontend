import {MapLayerDescription} from './map-layer-description';
/**
 * Created by alex on 16.09.17.
 */
export class AvailableMapLayers {
  readonly list: MapLayerDescription[];

  constructor(list: MapLayerDescription[]) {
    this.list = list;
  }
}
