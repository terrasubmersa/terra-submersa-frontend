import {LatLonCoords} from './lat-lon-coord';
import {LatLonBBox} from './lat-lon-bbox';

export class TileSystemBoundaries {
  readonly minLevel: number;
  readonly maxLevel: number;
  readonly boundaries: LatLonBBox;

  constructor(minLevel: number, maxLevel: number, boundaries: LatLonBBox) {
    this.minLevel = minLevel;
    this.maxLevel = maxLevel;
    this.boundaries = boundaries;
  }
}
