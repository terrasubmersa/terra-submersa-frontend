import {LatLonCoords} from './lat-lon-coord';

export class LatLonBBox {
  readonly upperLeft: LatLonCoords;
  readonly lowerRight: LatLonCoords;

  constructor(upperLeft: LatLonCoords, lowerRight: LatLonCoords) {
    this.upperLeft = upperLeft;
    this.lowerRight = lowerRight;
  }
}
