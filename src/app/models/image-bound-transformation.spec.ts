/**
 * an image Bound with upper left and lower right coordinates + a roation angle in degrees
 */
import {LatLonCoords} from './lat-lon-coord';
import {ImageBoundTransformation} from './image-bound-transformation';

describe('ImageBoundTransformation', () => {


  describe('pointTop', () => {
    describe('0° rotation', () => {
      const box = new ImageBoundTransformation(
        new LatLonCoords(60, 30),
        40,
        20,
        0
      );

      it('w=0', () => {
        const p = box.pointTop(0);

        expect(p).toEqual(new LatLonCoords(80, 30));
      });

      it('w=-1', () => {
        const p = box.pointTop(-1);

        expect(p).toEqual(new LatLonCoords(80, 20));
      });

      it('w=+1', () => {
        const p = box.pointTop(+1);

        expect(p).toEqual(new LatLonCoords(80, 40));
      });
    });

    describe('-30° rotation', () => {
      const box = new ImageBoundTransformation(
        new LatLonCoords(60, 30),
        40,
        20,
        -30
      );

      it('w=0', () => {
        const p = box.pointTop(0);

        expect(p).toEqual(new LatLonCoords(77.32050807568878, 40));
      });

      it('w=-1', () => {
        const p = box.pointTop(-1);

        expect(p).toEqual(new LatLonCoords(82.32050807568878, 31.33974596215561));
      });

      it('w=+1', () => {
        const p = box.pointTop(+1);
        const pRight1 = box.pointRight(1);

        expect(p).toEqual(new LatLonCoords(72.32050807568878, 48.66025403784439));
        expect(p).toEqual(pRight1);

      });
    });

  });

  describe('pointRight', () => {
    describe('0° rotation', () => {
      const box = new ImageBoundTransformation(
        new LatLonCoords(60, 30),
        40,
        20,
        0
      );

      it('w=0', () => {
        const p = box.pointRight(0);

        expect(p).toEqual(new LatLonCoords(60, 40));
      });

      it('w=-1', () => {
        const p = box.pointRight(-1);

        expect(p).toEqual(new LatLonCoords(40, 40));
      });

      it('w=+1', () => {
        const p = box.pointRight(+1);

        expect(p).toEqual(new LatLonCoords(80, 40));
      });
    });

    describe('-30° rotation', () => {
      const box = new ImageBoundTransformation(
        new LatLonCoords(60, 30),
        40,
        20,
        -30
      );

      it('w=0', () => {
        const p = box.pointRight(0);

        expect(p).toEqual(new LatLonCoords(55, 38.66025403784439));
      });

      it('w=-1', () => {
        const p = box.pointRight(-1);

        expect(p).toEqual(new LatLonCoords(37.67949192431122, 28.66025403784439));
      });

      it('w=+1', () => {
        const p = box.pointRight(+1);
        const pTop1 = box.pointTop(+1);

        expect(p).toEqual(new LatLonCoords(72.32050807568878, 48.66025403784439));
        expect(p).toEqual(pTop1);
      });
    });

  });

  describe('pointBottom', () => {
    describe('0° rotation', () => {
      const box = new ImageBoundTransformation(
        new LatLonCoords(60, 30),
        40,
        20,
        0
      );

      it('w=0', () => {
        const p = box.pointBottom(0);

        expect(p).toEqual(new LatLonCoords(40, 30));
      });

      it('w=-1', () => {
        const p = box.pointBottom(-1);

        expect(p).toEqual(new LatLonCoords(40, 20));
      });

      it('w=+1', () => {
        const p = box.pointBottom(+1);

        expect(p).toEqual(new LatLonCoords(40, 40));
      });
    });

    describe('-30° rotation', () => {
      const box = new ImageBoundTransformation(
        new LatLonCoords(60, 30),
        40,
        20,
        -30
      );

      it('w=0', () => {
        const p = box.pointBottom(0);

        expect(p).toEqual(new LatLonCoords(42.67949192431122, 20));
      });

      it('w=-1', () => {
        const p = box.pointBottom(-1);

        expect(p).toEqual(new LatLonCoords(47.67949192431122, 11.339745962155613));
      });

      it('w=+1', () => {
        const p = box.pointBottom(+1);

        expect(p).toEqual(new LatLonCoords(37.67949192431122, 28.66025403784439));
      });
    });
  });

  describe('pointLeft', () => {
    describe('0° rotation', () => {
      const box = new ImageBoundTransformation(
        new LatLonCoords(60, 30),
        40,
        20,
        0
      );

      it('w=0', () => {
        const p = box.pointLeft(0);

        expect(p).toEqual(new LatLonCoords(60, 20));
      });

      it('w=-1', () => {
        const p = box.pointLeft(-1);

        expect(p).toEqual(new LatLonCoords(40, 20));
      });

      it('w=+1', () => {
        const p = box.pointLeft(+1);

        expect(p).toEqual(new LatLonCoords(80, 20));
      });
    });

    describe('-30° rotation', () => {
      const box = new ImageBoundTransformation(
        new LatLonCoords(60, 30),
        40,
        20,
        -30
      );

      it('w=0', () => {
        const p = box.pointLeft(0);

        expect(p).toEqual(new LatLonCoords(65, 21.33974596215561));
      });

      it('w=-1', () => {
        const p = box.pointLeft(-1);
        const pTop = box.pointBottom(-1);

        expect(p).toEqual(pTop);
      });

      it('w=+1', () => {
        const p = box.pointLeft(+1);
        const pBottom = box.pointTop(-1);

        expect(p).toEqual(pBottom);

      });
    });
  });
});
