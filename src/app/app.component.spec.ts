import {async, TestBed} from '@angular/core/testing';

import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {selectedMapLayersReducer} from './store/selected-map-layers-reducer';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MdButtonModule, MdIconModule, MdMenuModule, MdSlideToggleModule} from '@angular/material';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        StoreModule.forRoot({
          selectedMapLayers: selectedMapLayersReducer,
        }),
        HttpClientTestingModule,
        MdMenuModule,
        MdButtonModule,
        MdSlideToggleModule,
        MdIconModule,
        NoopAnimationsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Terra Submersa data viewer');
  }));


});
