import {BrowserModule} from '@angular/platform-browser';
import {StoreModule} from '@ngrx/store';
import {HttpClientModule} from '@angular/common/http';

import {selectedMapLayersReducer} from './store/selected-map-layers-reducer';
import {
  MdButtonModule, MdCheckboxModule, MdDialogModule, MdIconModule, MdMenuModule,
  MdSlideToggleModule, MdToolbarModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

export const importedModules = [
  BrowserModule,
  StoreModule.forRoot({
    selectedMapLayers: selectedMapLayersReducer
  }),
  HttpClientModule,
  MdButtonModule,
  MdCheckboxModule,
  MdMenuModule,
  MdSlideToggleModule,
  MdIconModule,
  BrowserAnimationsModule,
  MdDialogModule,
  MdToolbarModule
];
