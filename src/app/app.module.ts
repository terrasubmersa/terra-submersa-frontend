import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MapBaseComponent} from './components/map-base/map-base.component';
import {TsFooterComponent} from './components/ts-footer/ts-footer.component';
import {TsHeaderComponent} from './components/ts-header/ts-header.component';
import {MapLayerSelectorComponent} from './components/map-layer-selector/map-layer-selector.component';
import 'hammerjs';
import {MenuActionsComponent} from './components/menu-actions/menu-actions.component';
import {importedModules} from './imported-modules';

@NgModule({
  declarations: [
    AppComponent,
    MapBaseComponent,
    TsFooterComponent,
    TsHeaderComponent,
    MapLayerSelectorComponent,
    MenuActionsComponent
  ],
  imports: importedModules,
  entryComponents: [
    MapLayerSelectorComponent
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
