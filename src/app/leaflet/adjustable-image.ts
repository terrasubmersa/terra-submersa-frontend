import {ImageBoundTransformation} from '../models/image-bound-transformation';
import * as L from 'leaflet';
import 'leaflet-imageoverlay-rotated';
import 'leaflet-rotatedmarker';
import {LatLonCoords} from '../models/lat-lon-coord';
import {ImageBoundTransformerService} from '../services/image-bound-transformer.service';
import {LatLng, Marker} from 'leaflet';

const adjustIcons = {
  rotate: new L.Icon({
    iconUrl: 'assets/icons/ic_replay_black_24dp_2x.png',
    iconSize: [48, 48],
    iconAnchor: [24, 50],
    popupAnchor: [24, 0]
  }),
  moveLeft: new L.Icon({
    iconUrl: 'assets/icons/ic_keyboard_arrow_left_black_24dp_2x.png',
    iconSize: [48, 48],
    iconAnchor: [24, 25],
    popupAnchor: [24, 0]
  }),
  moveUp: new L.Icon({
    iconUrl: 'assets/icons/ic_keyboard_arrow_up_black_24dp_2x.png',
    iconSize: [48, 48],
    iconAnchor: [24, 25],
    popupAnchor: [24, 0]
  }),
  moveRight: new L.Icon({
    iconUrl: 'assets/icons/ic_keyboard_arrow_right_black_24dp_2x.png',
    iconSize: [48, 48],
    iconAnchor: [0, 23],
    popupAnchor: [0, 24]
  }),
  moveDown: new L.Icon({
    iconUrl: 'assets/icons/ic_keyboard_arrow_down_black_24dp_2x.png',
    iconSize: [48, 48],
    iconAnchor: [24, 25],
    popupAnchor: [24, 0]
  }),
};

class AdjustableImage {
  readonly imagePath: string;
  transformation: ImageBoundTransformation;

  ibtService = new ImageBoundTransformerService();

  static latLonCoords2leafletLatLng(llc: LatLonCoords) {
    return new L.LatLng(llc.lat, llc.lon);
  }

  static leafletLatLng2latLonCoords(ll: LatLng) {
    return new LatLonCoords(ll.lat, ll.lng);
  }

  constructor(imagePath: string, transformation: ImageBoundTransformation) {
    this.imagePath = imagePath;
    this.transformation = transformation;
  }


  addToLayer(map: any) {
    const self = this;
    const pointUpperLeft = AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointTop(-1));
    const pointLowerRight = AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointLeft(-1));


    function dragTop() {
      self.transformation = self.ibtService.moveTop(
        self.transformation,
        AdjustableImage.leafletLatLng2latLonCoords(markers.get('top').getLatLng())
      );
      repositionImage();
    }

    function dragRight() {
      self.transformation = self.ibtService.moveRight(
        self.transformation,
        AdjustableImage.leafletLatLng2latLonCoords(markers.get('right').getLatLng())
      );
      repositionImage();
    }

    function dragBottom() {
      self.transformation = self.ibtService.moveBottom(
        self.transformation,
        AdjustableImage.leafletLatLng2latLonCoords(markers.get('bottom').getLatLng())
      );
      repositionImage();
    }

    function dragLeft() {
      self.transformation = self.ibtService.moveLeft(
        self.transformation,
        AdjustableImage.leafletLatLng2latLonCoords(markers.get('left').getLatLng())
      );
      repositionImage();
    }

    function dragRotateTop() {
      self.transformation = self.ibtService.rotateTop(
        self.transformation,
        AdjustableImage.leafletLatLng2latLonCoords(markers.get('rotateTop').getLatLng())
      );
      repositionImage();
    }


    const markers = new Map<string, Marker>();
    markers.set('top', L.marker(
      AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointTop(0)),
      {icon: adjustIcons.moveUp, draggable: true}
      )
        .addTo(map)
        .on('drag', dragTop)
    );
    markers.set('right', L.marker(
      AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointRight(0)),
      {icon: adjustIcons.moveRight, draggable: true}
      )
        .addTo(map)
        .on('drag', dragRight)
    );
    markers.set('bottom', L.marker(
      AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointBottom(0)),
      {icon: adjustIcons.moveDown, draggable: true}
      )
        .addTo(map)
        .on('drag', dragBottom)
    );

    markers.set('left',L.marker(
      AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointLeft(0)),
      {icon: adjustIcons.moveLeft, draggable: true}
    )
      .addTo(map)
      .on('drag', dragLeft)
    );
    markers.set('rotateTop', L.marker(
      AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointTop(0)),
      {icon: adjustIcons.rotate, draggable: true}
    )
      .addTo(map)
      .on('drag', dragRotateTop)
    );

    for (const marker of Array.from(markers.values())) {
      marker.on('dragend', () => console.log(self.transformation));
    }

    function repositionImage() {
      overlay.reposition(
        AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointTop(-1)),
        AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointTop(1)),
        AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointLeft(-1))
      );
      markers.get('top').setLatLng(AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointTop(0)));
      markers.get('rotateTop').setLatLng(AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointTop(0)));
      markers.get('right').setLatLng(AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointRight(0)));
      markers.get('bottom').setLatLng(AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointBottom(0)));
      markers.get('left').setLatLng(AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointLeft(0)));
    }


    const overlay = L.imageOverlay.rotated(this.imagePath, pointUpperLeft,
      AdjustableImage.latLonCoords2leafletLatLng(self.transformation.pointTop(1)),
      pointLowerRight, {
        opacity: 0.4,
        interactive: true,
        attribution: '&copy; Terra Submersa</a>'
      });
    map.addLayer(overlay);
  }
}

export {AdjustableImage}
