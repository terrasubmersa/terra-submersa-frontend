import { browser, by, element } from 'protractor';

export class TerraSubmersaFrontendPage {
  navigateTo() {
    return browser.get('/');
  }

  getHeaderText() {
    return element(by.css('app-root ts-header')).getText();
  }
}
