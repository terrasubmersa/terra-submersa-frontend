# TerraSubmersaFrontend

Based on AngularJS, this component is to display a map with superimposed measure artefact


## The dev side of it
### Dev data

Only a small area is saved within the project. It correspond to a Kilada Bay OSM tile, with coordinates
z=16 x=36978 y=25412


### Development server

    nvm use 8
    npm install -g @angular/cli
    npm install

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 


### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Deploy
    ng build --prod
    rm -r dist/assets/dev-data
    docker build -t terra-submersa-frontend .
    docker tag terra-submersa-frontend alexmass/terra-submersa-frontend
    #docker login
    docker push alexmass/terra-submersa-frontend

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

